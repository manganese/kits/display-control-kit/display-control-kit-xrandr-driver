import process from 'node:process';
import { exec } from 'node:child_process';
import { DEFAULT_DISPLAY, XRANDR } from './constants';

const executeCommand = (command: string): Promise<string> => {
  console.log(`Executing: ${command}`);

  return new Promise((resolve, reject) =>
    exec(command, (error, stdout, stderr) => {
      if (error) {
        console.error(error, stderr);

        reject({
          error,
          stderr,
        });

        return;
      }

      resolve(stdout.toString());
    })
  );
};

export const getCommandEnvironmentPrefix = () => {
  const display = process.env.DISPLAY || DEFAULT_DISPLAY;

  return `export DISPLAY=${display};`;
};

export const buildCommand = (command: string) =>
  `${getCommandEnvironmentPrefix()} ${XRANDR} ${command}`;

export const buildOutputCommand = (outputName: string, outputCommand: string) =>
  buildCommand(`--output ${outputName} ${outputCommand}`);

export const executeOutputCommand = (
  outputName: string,
  outputCommand: string
) => {
  const command = buildOutputCommand(outputName, outputCommand);

  return executeCommand(command);
};

export const executeOutputAuto = (outputName: string) =>
  executeOutputCommand(outputName, '--auto');
export const executeOutputOff = (outputName: string) =>
  executeOutputCommand(outputName, '--off');

export default {
  on: async (outputName: string) => {
    await executeOutputAuto(outputName);
  },
  off: async (outputName: string) => {
    await executeOutputOff(outputName);
  },
};
